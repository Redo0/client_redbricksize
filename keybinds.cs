
if(!$rbsMadeKeybinds){
	$remapDivision[$remapCount] = "RBS Brick Size";
	
	$remapName[$remapCount]     = "Increase Brick Height 3";
	$remapCmd[$remapCount]      = "rbsIncreaseBrickHeight3";
	$remapCount++;
	$remapName[$remapCount]     = "Decrease Brick Height 3";
	$remapCmd[$remapCount]      = "rbsDecreaseBrickHeight3";
	$remapCount++;
	$remapName[$remapCount]     = "Increase Brick Height 1";
	$remapCmd[$remapCount]      = "rbsIncreaseBrickHeight1";
	$remapCount++;
	$remapName[$remapCount]     = "Decrease Brick Height 1";
	$remapCmd[$remapCount]      = "rbsDecreaseBrickHeight1";
	$remapCount++;
	$remapName[$remapCount]     = "Increase Brick Width";
	$remapCmd[$remapCount]      = "rbsIncreaseBrickWidth";
	$remapCount++;
	$remapName[$remapCount]     = "Decrease Brick Width";
	$remapCmd[$remapCount]      = "rbsDecreaseBrickWidth";
	$remapCount++;
	$remapName[$remapCount]     = "Increase Brick Depth";
	$remapCmd[$remapCount]      = "rbsIncreaseBrickDepth";
	$remapCount++;
	$remapName[$remapCount]     = "Decrease Brick Depth";
	$remapCmd[$remapCount]      = "rbsDecreaseBrickDepth";
	$remapCount++;
	
	$rbsMadeKeybinds=1;
}

function rbsIncreaseBrickHeight3(%val){ if(%val){ rbsAddBrickSizeSuper("0 0 3" ); shiftBrickDown   (1); shiftBrickUp     (1); shiftBrickDown   (0); shiftBrickUp     (0); } }
function rbsDecreaseBrickHeight3(%val){ if(%val){ rbsAddBrickSizeSuper("0 0 -3"); shiftBrickUp     (1); shiftBrickDown   (1); shiftBrickUp     (0); shiftBrickDown   (0); } }
function rbsIncreaseBrickHeight1(%val){ if(%val){ rbsAddBrickSizeSuper("0 0 1" ); shiftBrickDown   (1); shiftBrickUp     (1); shiftBrickDown   (0); shiftBrickUp     (0); } }
function rbsDecreaseBrickHeight1(%val){ if(%val){ rbsAddBrickSizeSuper("0 0 -1"); shiftBrickUp     (1); shiftBrickDown   (1); shiftBrickUp     (0); shiftBrickDown   (0); } }
function rbsIncreaseBrickWidth  (%val){ if(%val){ rbsAddBrickSizeSuper("0 1 0" ); shiftBrickLeft   (1); shiftBrickRight  (1); shiftBrickLeft   (0); shiftBrickRight  (0); } }
function rbsDecreaseBrickWidth  (%val){ if(%val){ rbsAddBrickSizeSuper("0 -1 0"); shiftBrickRight  (1); shiftBrickLeft   (1); shiftBrickRight  (0); shiftBrickLeft   (0); } }
function rbsIncreaseBrickDepth  (%val){ if(%val){ rbsAddBrickSizeSuper("1 0 0" ); shiftBrickTowards(1); shiftBrickAway   (1); shiftBrickTowards(0); shiftBrickAway   (0); } }
function rbsDecreaseBrickDepth  (%val){ if(%val){ rbsAddBrickSizeSuper("-1 0 0"); shiftBrickAway   (1); shiftBrickTowards(1); shiftBrickAway   (0); shiftBrickTowards(0); } }
