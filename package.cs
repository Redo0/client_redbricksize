
//basically finding every function that could possibly change the ghost brick in order to know whenever it changes
package rbsBrickKeys{
	function clientCmdMissionStartPhase3(%a, %map){
		rbsFindBrickSizes();
		parent::clientCmdMissionStartPhase3(%a, %map);
	}
	
	function PlayGui::createInvHud(%this){
		parent::createInvHud(%this);
		if($ScrollMode==$SCROLLMODE_BRICKS){
			rbsBrickChanged($CurrScrollBrickSlot);
		}
	}
	
	function handleSetInvData(%msgType, %msgString, %slot, %data){
		parent::handleSetInvData(%msgType, %msgString, %slot, %data);
		if($ScrollMode==$SCROLLMODE_BRICKS && $CurrScrollBrickSlot==%slot){
			rbsBrickChanged(%slot);
		}
	}
	
	function useFirstSlot  (%val){ parent::useFirstSlot  (%val); if(%val){ %slot=0; rbsBrickChanged(%slot); } }
	function useSecondSlot (%val){ parent::useSecondSlot (%val); if(%val){ %slot=1; rbsBrickChanged(%slot); } }
	function useThirdSlot  (%val){ parent::useThirdSlot  (%val); if(%val){ %slot=2; rbsBrickChanged(%slot); } }
	function useFourthSlot (%val){ parent::useFourthSlot (%val); if(%val){ %slot=3; rbsBrickChanged(%slot); } }
	function useFifthSlot  (%val){ parent::useFifthSlot  (%val); if(%val){ %slot=4; rbsBrickChanged(%slot); } }
	function useSixthSlot  (%val){ parent::useSixthSlot  (%val); if(%val){ %slot=5; rbsBrickChanged(%slot); } }
	function useSeventhSlot(%val){ parent::useSeventhSlot(%val); if(%val){ %slot=6; rbsBrickChanged(%slot); } }
	function useEighthSlot (%val){ parent::useEighthSlot (%val); if(%val){ %slot=7; rbsBrickChanged(%slot); } }
	function useNinthSlot  (%val){ parent::useNinthSlot  (%val); if(%val){ %slot=8; rbsBrickChanged(%slot); } }
	function useTenthSlot  (%val){ parent::useTenthSlot  (%val); if(%val){ %slot=9; rbsBrickChanged(%slot); } }
	
	function clientCmdSetActiveBrick(%slot){ parent::clientCmdSetActiveBrick(%slot); rbsBrickChanged(%slot); }
	
	function useBricks(%val){
		parent::useBricks(%val);
		if(%val){
			if($InvData[$CurrScrollBrickSlot]!=-1.0){ rbsBrickChanged($CurrScrollBrickSlot); }
			else { rbsBrickChanged(0); }
		}
	}
	
	function scrollInventory(%val){
		parent::scrollInventory(%val);
		if($ScrollMode==$SCROLLMODE_BRICKS || !$ScrollMode){
			rbsBrickChanged($CurrScrollBrickSlot);
		}
	}
	
	function BSD_RightClickIcon(%data){
		parent::BSD_RightClickIcon(%data);
		rbsBrickChanged(0,%data);
	}
};
activatePackage(rbsBrickKeys);
