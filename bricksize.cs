
exec("./keybinds.cs");
exec("./package.cs");

function rbsGetBrickSize(%data){
	return %data.brickSizeX SPC %data.brickSizeY SPC %data.brickSizeZ;
}

function rbsFindBrickSizes(){
	deleteVariables("$rbsBrickSizes*");
	deleteVariables("$rbsBrickWireSizes*");
	
	echo("rbs finding brick sizes");
	
	%dbCount = getDatablockGroupSize();
	for(%i=0; %i<%dbCount; %i++){
		%db = getDataBlock(%i);
		
		if(%db.getClassName()$="fxDtsBrickData"){
			if(%db.uiName!$="" && !%db.hasPrint){
				%size = rbsGetBrickSize(%db);
				
				if(%db.category$="Bricks" || %db.category$="Plates" || %db.category$="Baseplates" || %db.category$="Halfbricks"){
					if(
						strstr(%db.uiName, "Tile")==-1 && 
						strstr(%db.uiName, "Block")==-1 && 
						strstr(%db.uiName, "Brick Upright")==-1 && 
						strstr(%db.uiName, "O Brick")==-1 &&
						strstr(%db.uiName, "0.5")==-1 &&
						strstr(%db.subCategory, "Tetris")==-1 &&
						!(strstr(%db.uiName, "Flat")!=-1 && strstr(%db.uiName, "Brick")!=-1)
					){
						if(!$rbsBrickSizes[%size]){
							$rbsBrickSizes[%size] = %db;
						}
					}
				}else if(%db.category$="Logic Bricks"){
					if(!$rbsBrickWireSizes[%size]){
						$rbsBrickWireSizes[%size] = %db;
					}
				}
			}
		}
	}
}

function rbsUseBrick(%db){
	if(!isObject(%db))return;
	%db = %db.getId();
	
	$rbsCurrBrick = %db;
	
	if(false){
		%slot=$CurrScrollBrickSlot;
		
		$BSD_InvData[%slot] = %db;
		
		$HUD_BrickIcon[%slot].setBitmap(""); //clear the slot image so the server can fill it
		$InvData[%slot] = -1.0; 
		commandToServer('BuyBrick', %slot, $BSD_InvData[%slot]); //tell the server to give us this brick
	}else{
		commandToServer('InstantUseBrick', %db);
	}
}

function rbsUseBrickSize(%size, %wire){
	if(!%wire){
		%db = $rbsBrickSizes[%size];
	}else{
		%db = $rbsBrickWireSizes[%size];
	}
	if(%db){
		rbsUseBrick(%db);
		return 1;
	}
	return 0;
}

function rbsAddBrickSizeSuper(%size){
	if($SuperShift){
		%size = getWord(%size, 0)*8 SPC getWord(%size, 1)*8 SPC getWord(%size, 2)*5;
	}
	rbsAddBrickSize(%size);
}

function rbsAddBrickSize(%size){
	%size=vectorAdd(%size, $rbsCurrBrickSize);
	if(getWord(%size,0)<=0 || getWord(%size,1)<=0 || getWord(%size,2)<=0){
		%size=$rbsCurrBrickSize;
	}
	rbsUseBrickSize(%size, $rbsCurrBrick.category$="Logic Bricks");
	$rbsCurrBrickSize = %size;
}

function rbsBrickChanged(%slot,%data){
	if(!%data){ %data=$InvData[%slot]; }
	$rbsCurrBrickSize = rbsGetBrickSize(%data);
	$rbsCurrBrick = %data;
}
